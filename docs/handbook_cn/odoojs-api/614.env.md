### 使用 env

1. 在 odoo-api 中 使用 env, 与在 odoo-rpc 中一样
2. 实际上 api.env 就是 api.rpc.env
3. 可以通过 api.env.model 方法, 使用所有的模型方法
4. 可通过 api.env.ref 方法, 获取 xml_ref 的 模型名和 ID

```
import api from '@/odoojs/index.js'
async function test_env() {
  const uid = api.env.uid
  const user = api.env.user
  const context = api.env.context
  const Model = api.env.model('res.users')

  const ref_result = await api.env.ref('base.user_admin')

}
```
