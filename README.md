## 联系我们

1. QQ 群 2684913
2. odoojs@outlook.com
3. 微信号 winboy99 醉桥. 添加时, 请注明 odoojs
4. 北京醉桥 负责维护 odoojs 的所有代码
5. 欢迎有志于 odoo 前后端分离项目的 个人/团队, 共同维护 odoojs
6. odoojs 团队提供 odoojs 前后端分离 解决方案付费培训服务, 请加入 qq 群, 联系群群管理员

## 最近更新 2023-10-30

1. 将代码移动到 文件夹 /demo-project/odoojs-demo-vue-antd-v1
2. 整理 docs 文件夹(进行中 doing...)

## odoojs 介绍

1. odoojs 是 odoo 前后端分离解决方案
2. odoojs 以 odoo 为服务端. 全新实现独立的前端
3. 核心 node package: odoojs-rpc, odoojs-api
4. 不限前端架构. 如 vue, react
5. 不限前端 ui 库. 如 ant-design-vue, uni-uview-plus
6. 支持多语言.

## odoojs 需要您的支持

1. 投资 odoojs, 建立核心管理团队. 长期服务于 odoo 前后端分离解决方案, 以及中国本地化 erp 的研发.
2. 邀请 odoojs 成为您的技术合作伙伴. 由 odoojs 为您的 ERP 产品提供技术服务.
3. 使用 odoojs demo 源码. 直接给 odoojs 的作者"醉桥"发红包.
4. 参加 odoojs 茶馆活动. 一起参与 odoojs 的发展. 仅付点茶水费即可.
5. odoojs 邀请您成为 odoojs 的合作伙伴. 共同完善 odoojs demo 代码.
6. odoojs 目前需要一位资深前端专家的帮助. 评估现有 odoojs 的技术架构.
7. odoojs 需要一位合作者, 建立和发展 odoojs 社区.

## odoojs 发展路线

1. 建立 odoojs 开源服务模式. 逐步形成 odoojs 社区
2. 形成中国本地化 ERP 产品.
3. 逐步探索 DIY(Do It Yourself)开发方式, 形成 DIY ERP 的低代码开发模式.
4. 依托 odoojs 社区. 建立对 odoo 开发团队, odoo 末端用户的服务培训模式

## odoojs 的现状(2023-10-30)

1. odoojs 由醉桥维护. 微信号 winboy99. 邮箱 odoojs@outlook.com
2. 目前国内有几个团队在使用 odoojs 解决方案.
3. odoojs 得到同行的认可.
4. 在 [bilibili](https://www.bilibili.com/) 搜索 "醉桥 odoo odoojs" 可获得相关的视频

## odoojs 适用场景

1. odoo 实施服务团队, 基于官方 odoo 功能, 为客户提供标准化服务. 使用 odoojs 自定义前端部署, 适配客户个性化前端需求.
2. odoo 实施开发团队, 开发自己的 ERP 产品, 使用 odoojs 前后端分离解决方案. 去 odoo 化, 形成独立的产品.
3. 末端用户, 使用 odoo 为自己公司建立服务平台. 自行组建技术队伍. 使用 odoojs 前后端分离解决方案, 优化技术路线, 迭代式开发, 随需而动, 满足个性化业务场景.
4. 跨平台, 多系统集成. odoo 只是一个子业务系统. 使用 odoojs 作为集成 odoo 的桥梁.
5. 基于 odoo 做业务管理. 使用 odoojs 扩展搭建 业务分析平台. 扩展 BI 等前端应用.
6. 基于 odoo 做业务管理. 使用 odoojs 扩展移动端应用.

## odoojs 发展历史

1. 第一阶段. 独立搭建 web 服务, 与 odoo 通过 xmlrpc 通讯. 自定义 ui 界面.
2. 第二阶段. 使用 odoo 已有的 web 服务. 扩展 对外接口. 自定义 ui 界面.
3. 第三阶段. 使用 odoo 已有的 web 服务. 使用 odoo 已有对外 jsonrpc 接口.
   前端获取 odoo 的所有 menu/action/view, 进行解析渲染.
4. 第四阶段. 扬弃官方 odoo 在服务端定义 menu/action/view 的设计思路. 在前端定义所有的 menu/action/view.
   官方的 menu/action/view, 与业务数据的耦合性太高. 不符合前后端分离的设计思想.
   在前端定义 menu/action/view. 这样, 官方 odoo 成为一个独立的业务数据服务.
   而前端, 完全自主设计一切 ui/ue 要素.
   这个阶段, 前后端分离解决方案, 正式形成. 并命名为 odoojs 前后端分离架构.
5. 第五阶段. 整理前端代码. 形成 odoojs-rpc, odoojs-api 两个标准模块. 并标准化前端 demo 项目的结构.
   odoojs-rpc 负责与 odoo 的数据接口交互, 以及编辑页面临时数据的暂存.
   odoojs-api 负责管理所有的 menu/action/view, 多语言翻译. 以及 addons 的管理.
   前端 demo 项目中, 形成适配 odoo 的标准组件.
6. 至此, odoojs 前后端分离解决方案, 成型.

## demo project

1. 以下几个 demo, 演示如何使用 odoojs
2. [文件夹 /demo-project/odoojs-demo-vue-antd-v1](https://gitee.com/odoowww/odoojs/tree/master/demo-project/odoojs-demo-vue-antd-v1) 是一个 odoojs demo 项目. 使用: [vue3](https://vuejs.org/), [Vue CLI](https://cli.vuejs.org/), [antdv](https://www.antdv.com/docs/vue/introduce)
3. [odoojs demo PC 端](https://gitee.com/odoowww/odoojs-demo-vue-antd) 使用: [vue3](https://vuejs.org/), [Vite](https://vitejs.dev/), [antdv](https://www.antdv.com/docs/vue/introduce)
4. [odoojs demo 移动端](https://gitee.com/odoowww/odoojs-demo-vue-uni-uview-plus) 使用: [vue3](https://vuejs.org/), [Vite](https://vitejs.dev/), [uniapp](https://uniapp.dcloud.net.cn/), [uview-plus](https://uiadmin.net/uview-plus/)
5. [odoojs demo 仅使用 odoojs-rpc](https://gitee.com/odoowww/odoojs-demo-vue-uni-simple) 使用: [vue3](https://vuejs.org/), [Vite](https://vitejs.dev/), [uniapp](https://uniapp.dcloud.net.cn/), [uview-plus](https://uiadmin.net/uview-plus/)

## odoojs demo 项目 日常维护内容

1. 跟进官方 odoo 的迭代升级. 更新 odoojs-rpc, odoojs-api 两个标准模块.
2. 完善 demo 项目中的标准组件.
3. 逐步复刻 官方 odoo. 形成可交付的 产品.

## odoojs 技术原理

### 技术路线

1. odoo 官方源码做服务端
2. odoojs 实现前后端分离
3. odoojs 前端 暂时选择 vue 架构. 但不限. 可以选择 react
4. odoojs 前端 暂时选择 UI 库 antd-vue. 但不限. 可选择其他常用的 UI 库
5. odoojs 完全支持 移动 web 端. 只需选择适用的 UI 库即可
6. odoojs 完全支持 移动端 app. 只需直接使用 odoojs-rpc 做 服务端访问接口即可
7. odoojs 完全支持小程序等特定的客户端. 只需要使用 小程序专用的组件即可

### odoojs 流程

1. odoojs 前端程序, 启动后, 首先加载所有自定义的 menu, action, view.
2. 渲染 menu.
3. 在一个 menu 选中后, 获取到对应的 action.
4. 根据 action, 获取 相应的 tree view 或者 form view.
5. 根据 view, 获取对应的 model, fields. 以及 html 要素.
6. 根据 model, fields 从 odoo 服务端获取数据.
7. 获取到的数据, 在 view 中, 各 field 自行使用相应组件进行渲染显示.
8. view 中的 field, 支持 readonly, visible, required, domain 等属性.

### odoojs 编辑页面的流程

1. 需要编辑数据时, 将 view 设置为编辑状态.
2. 在编辑状态下, 创建一个 editmodel.
3. editmodel 由 odoojs-rpc 进行管理, 在前端页面中无需额外关心.
4. 该 editmodel 管理所有的编辑中的数据.
5. view 中某个字段 field 编辑后, 触发 onchange.
6. onchange 在 editmodel 中进行排队. 以确保 onchange 顺序依次触发.
7. onchange 访问 odoo 服务端, 获取数据. 更新到 editmodel. 并返回到 view.
8. view 渲染数据.
9. view 编辑完成, 触发 commit.
10. commit 与 onchang 一样 在 editmodel 中排队.
11. commit 访问 odoo 服务端, 发送 create or write 请求, 更新数据到服务端.
12. commit 之后, 销毁 editmodel, view 回到只读状态.
13. view 发送 read 请求, 重新获取数据, 并渲染到 view 中.

### odoojs one2many 字段的处理

1. main view 渲染显示数据.
2. main view 中, 各 field 由相应的 field 组件进行渲染.
3. one2many 字段的 sub tree view, sub form view 在 main view 中已定义
4. main view 读取数据时, 已经嵌套获取 one2many 字段的数据
5. one2many 字段 sub tree view 渲染数据.
6. one2many 字段 sub form view 以弹窗方式 显示单条数据.

### odoojs one2many 字段的编辑处理

1. main view 进入编辑状态.
2. one2many 字段 sub tree view 为编辑状态, 显示 create 按钮.
3. pick one sub record or new sub record, 创建 sub form view.
4. sub form view 创建 sub editmodel.
5. sub form view 触发 onchange, 访问 odoo 服务端, 更新到 sub editmodel.
6. sub form view 触发 commit. 销毁 sub editmodel. 更新数据到 sub tree view.
7. sub tree view 触发 main view 的 onchage.
8. main view 的 commit 之前, 从 sub tree view 获取 one2many 字段的数据.
9. main view 触发 commit
